/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listcomparator;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;




/**
 *
 * @author MaxKorwinCejrowski
 */
public class ListComparator {
       public static List<Person> listaOsob;
       public static List<PersonWithCompare> listaOsobCompare;

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        listaOsob = new ArrayList<>();
        listaOsobCompare = new ArrayList<>();
        
        listaOsob.add(new Person("Kacper","Lenczuk","20.08.1995"));
        listaOsob.add(new Person("Anna","Maria","Wesołowska","22.01.1955"));
        listaOsob.add(new Person("Bartłomiej","Andrzej","Berling","07.06.1995"));
        listaOsob.add(new Person("Zenon","Laskowik","11.11.1911"));
        
        listaOsobCompare.add(new PersonWithCompare("Kacper","Lenczuk","20.08.1995"));
        listaOsobCompare.add(new PersonWithCompare("Anna","Maria","Wesołowska","22.01.1955"));
        listaOsobCompare.add(new PersonWithCompare("Bartłomiej","Andrzej","Berling","07.06.1995"));
        listaOsobCompare.add(new PersonWithCompare("Kacper","Kowalik","20.08.1995"));
        listaOsobCompare.add(new PersonWithCompare("Zenon","Laskowik","11.11.1911"));
        
        System.out.println("Unsorted: /n");
        listaOsob.forEach((p) -> {
            System.out.println(p.toString());
           });
        
        Collections.sort(listaOsob, new NameComparator());
        
        System.out.println("Name sorted: /n");
        listaOsob.forEach((p) -> {
            System.out.println(p.toString());
           });
        
        Collections.sort(listaOsob, new SurnameComparator());
        
        System.out.println("Surname sorted: /n");
        listaOsob.forEach((p) -> {
            System.out.println(p.toString());
           });
        
        System.out.println("/n");
        System.out.println("Person with compare: /n");
        System.out.println("Unsorted: ");
        listaOsobCompare.forEach((p) -> {
            System.out.println(p.toString());
           });
        
        Collections.sort(listaOsobCompare);
        //Lambda expression
        System.out.println("Name sorted: ");
        listaOsobCompare.forEach((p) -> {
            System.out.println(p.toString());
           });
        
        //odwołanie do metody
        System.out.println("--------------------------------------------------------");
        listaOsobCompare.forEach(System.out::println);
        
        //Wyłuskanie pól klasy wewnetrznej
        Person xxx = listaOsob.get(0);
        System.out.println(xxx.getAdres().getCityName());
        
        
            ScriptEngine eng = new ScriptEngineManager().getEngineByName("JavaScript");
            
            String eq = "10+(-10)";
            
            try
            {
                System.out.println( eng.eval(eq));
            }
            catch(ScriptException e)
            {
            }
            
    }
    
}
