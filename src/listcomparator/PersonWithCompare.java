/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listcomparator;

/**
 *
 * @author MaxKorwinCejrowski
 */
public class PersonWithCompare implements Comparable {
private String name;
    private String secondName;
    private String surname;
    private String brithDate;
    
    
    public PersonWithCompare(String name, String surname, String brithDate)
    {
        this.name = name;
        this.surname = surname;
        this.brithDate = brithDate;
    }
    
    public PersonWithCompare(String name, String secondName, String surname, String brithDate)
    { 
        this.name = name;
        this.secondName = secondName;
        this.surname = surname;
        this.brithDate = brithDate;        
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBrithDate(String brithDate) {
        this.brithDate = brithDate;
    }
    
    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSurname() {
        return surname;
    }

    public String getBrithDate() {
        return brithDate;
    }
    
    @Override
    public String toString()
    {
        StringBuilder build = new StringBuilder();
        build.append("Imie: ").append(name).append(" Nazwisko: ").append(surname).append(" Data ur. : ").append(brithDate);
        return build.toString();
    }
    @Override
    public int compareTo(Object o) {
    PersonWithCompare second = (PersonWithCompare) o;
    
    if(second==null)
        return 0;

    int wynik = this.name.compareTo(second.getName());
    
    if(wynik==0)
            return this.surname.compareTo(second.getSurname());
    else return wynik;
    }
    
}
