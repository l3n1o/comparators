/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listcomparator;

/**
 *
 * @author MaxKorwinCejrowski
 */
public class Person {
    
    class Address{
        protected String strName;
        protected int houseNumber;
        protected int flatNumber;
        protected String cityName;
        protected String postalCode;

        public String getStrName() {
            return strName;
        }

        public void setStrName(String strName) {
            this.strName = strName;
        }

        public int getHouseNumber() {
            return houseNumber;
        }

        public void setHouseNumber(int houseNumber) {
            this.houseNumber = houseNumber;
        }

        public int getFlatNumber() {
            return flatNumber;
        }

        public void setFlatNumber(int flatNumber) {
            this.flatNumber = flatNumber;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public Address(String strName, int houseNumber, String cityName, String postalCode){
            this.strName=strName;
            this.houseNumber = houseNumber;
            this.flatNumber = 0;
            this.cityName = cityName;
            this.postalCode = postalCode;
        }
                
        public Address(String strName, int houseNumber, int flatNumber, String cityName, String postalCode){
            this.strName=strName;
            this.houseNumber = houseNumber;
            this.flatNumber = flatNumber;
            this.cityName = cityName;
            this.postalCode = postalCode;
        }
        
        @Override
        public String toString(){
            StringBuilder build = new StringBuilder();
            build.append(strName)
                .append(" ").append(houseNumber)
                .append("/").append(flatNumber)
                .append(", ").append(cityName)
                .append(", ").append(postalCode);
        return build.toString();
        }
        
    }
    private String name;
    private String secondName;
    private String surname;
    private String brithDate;
    private Address adres;

    public Address getAdres() {
        return adres;
    }

    public void setAdres(Address adres) {
        this.adres = adres;
    }
    
    public Person(String name, String surname, String brithDate)
    {
        this.name = name;
        this.secondName="";
        this.surname = surname;
        this.brithDate = brithDate;
        adres = new Address("Hubala", 8, 9, "Polkowice", "59-100");
    }
    
    public Person(String name, String secondName, String surname, String brithDate)
    { 
        this.name = name;
        this.secondName = secondName;
        this.surname = surname;
        this.brithDate = brithDate;    
        adres = new Address("Hubala", 8, 9, "Polkowice", "59-100");
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBrithDate(String brithDate) {
        this.brithDate = brithDate;
    }
    
    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSurname() {
        return surname;
    }

    public String getBrithDate() {
        return brithDate;
    }
    
    @Override
    public String toString()
    {
        StringBuilder build = new StringBuilder();
        build.append("Imie: ").append(name)
                .append(" Nazwisko: ").append(surname)
                .append(" Data ur.: ").append(brithDate)
                .append(" Adres: ").append(adres.toString());
        return build.toString();
    }

}
